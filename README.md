# README #

Used to export trades from Bleutrade.com.

### How do I get set up? ###

Create a text file called config.json and put it in the same folder as you run this from:


`{
  "Key": "12345678901234567890123456789012",
  "Secret": "09876543210987654321098765432109"
}`


Replace the key and secret with your own from https://bleutrade.com/member/api_keys.