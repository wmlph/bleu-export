package main

import (
	"bitbucket.org/wmlph/bleutradeapi"
	"flag"
	"fmt"
	"log"
)

func main() {
	market := flag.String("market", "DOGE_BTC", "the market name you want to export, DOGE_BTC for example")
	Type := flag.String("type", "ALL", "The type of trade you require, ALL, OK, OPEN, or CANCELED")
	flag.Parse()

	bleu := bleutrade.New("config.json")
	orders, err := bleu.GetOrders(*market, *Type)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("OrderId, Exchange, Type, Quantity, QuantityRemaining, QuantityBaseTraded, Price, Status, Created, Comments")
	for k := range orders {
		v := orders[k]
		if v.Comments == "" {
			v.Comments = "No Comment Entered"
		}
		fmt.Printf("%s, %s,%s,%s,%s,%s,%s,%s,%s,%s\n", v.OrderId, v.Exchange, v.Type, v.Quantity, v.QuantityRemaining, v.QuantityBaseTraded, v.Price, v.Status, v.Created, v.Comments)
	}
}
